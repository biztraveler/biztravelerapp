import superagent from 'superagent';
export function uploadSuccess(data) {
  console.log('im in hereeeee', data);
  return {
    type: 'UPLOAD_SUCCESS',
    tripPicUrl: data.text,
  };
}

export function uploadFail(error) {
  return {
    type: 'UPLOAD_FAIL',
    error,
  };
}

export function uploadFileRequest(file) {
  console.log('i am the file in the action:', file);
  let data = new FormData();
  data.append('file', file);
  data.append('name', file.name);
  return (dispatch) => {
    // superagent.get('https://pokeapi.co/api/v2/pokemon/1')
    superagent.post(`${API_URL}/api/v1/image`)
    // superagent.post('http://localhost:8170/api/v1/image')
      // .field('image', file)
      .send(data)
      .then(res => {
        console.log('success', res);
        dispatch(uploadSuccess(res));
      })
      .catch(err => {
        console.log('fail');
        dispatch(uploadFail(err));
      });
  };
}
