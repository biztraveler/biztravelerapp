import superagent from 'superagent';

export const setTrips = trips => ({
  type: 'TRIPIT_GET',
  payload: trips,
});

export const fetchTrippitRequest (id) => ({
  return superagent.get(`${API_URL}/tripit/${id}`)
  .then((res) => {
    return store.dispatch(setTrips(res.body));
  })
})
