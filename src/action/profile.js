import superagent from 'superagent';

export const get = user => ({
  type: 'PROFILE_GET',
  payload: user,
});
export const getFail = (err) => ({
  type: 'PROFILE_GET_FAIL',
  payload: err,
});
export const getTripit = user => ({
  type: 'TRIPIT_GET',
  payload: user,
});
export const getTripitFail = (err) => ({
  type: 'TRIPIT_GET_FAIL',
  payload: err,
});
export const tripitRefresh = user => ({
  type: 'TRIPIT_REFRESH',
  payload: user,
});
export const tripitRefreshFail = (err) => ({
  type: 'TRIPIT_REFRESH_FAIL',
  payload: err,
});

export const getMyInfo = () => store => {
  console.log('using local server for teting, line 15 for deployed server');
  return superagent.get(`${__BTA_URL__}/api/v1/user/0`)
  // return superagent.get(`${__BTA_URL__}/api/v1/user/me`)
    .withCredentials()
    .then(res => {
      let data = {
        linkedin: res.body.profile,
        tripit: res.body.trips,
      };
      store.dispatch(get(data));
    })
    .catch((err) => {
      console.log('in my catch');
      store.dispatch(getFail(err));
    });
};
export const getMyTripitInfo = () => store => {
  console.log('using local server for teting, line 15 for deployed server');
  // return superagent.get(`${'http://localhost:8170'}/auth/tripit/init`)
  return superagent.get(`${__BTA_URL__}/auth/tripit/init`)
  // return superagent.get(`${__BTA_URL__}/api/v1/user/me`)
    .withCredentials()
    .then(res => {
      console.log('res from tripit', res);
      let data = {
        linkedin: res.body.profile,
        tripit: res.body,
      };
      store.dispatch(getTripit(data));
    })
    .catch((err) => {
      console.log('in my catch');
      store.dispatch(getTripitFail(err));
    });
};

export const refreshTrips = () => store => {
  return superagent.get(`${__BTA_URL__}/api/v1/tripit-sync`)
  .withCredentials()
  .then( res => {
    console.log(res);
    store.dispatch(tripitRefresh(res.body))
  })
  .catch( err=> {
    store.dispatch(tripitRefreshFail(err))
  })
}
