import superagent from 'superagent';

export function loginSuccess(data){
  return {
    type: 'LOGIN_SUCCESS',
    payload: data,
  };
}
export function loginFail(data){
  return {
    type: 'LOGIN_FAIL',
    payload: data,
  };
}

export function loginRequest(){
  return (dispatch) =>{
    superagent.get(`${__BTA_URL__}/auth/linkedin/init`)
      .then((res) => {
        console.log('login res', res);
        return dispatch(loginSuccess(res.body));
      })
      .catch((err) => {
        return dispatch(loginFail(err));
      });
  };
}

export function loginFlag() {
  return {
    type: 'LOGIN_FLAG',
    payload: true,
  };
}
