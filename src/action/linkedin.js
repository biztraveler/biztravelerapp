import superagent from 'superagent';

export const get = (user) => ({
  type: 'LINKEDIN_GET',
  payload: user,
});
export const getFail = (err) => ({
  type: 'LINKEDIN_GET_FAIL',
  payload: err,
});

export const getMyInfo = () => store => {
  console.log('using local server for teting, line 15 for deployed server');
  return superagent.get(`${'http://localhost:8170'}/api/v1/user/me`)
  // return superagent.get(`${__BTA_URL__}/api/v1/user/me`)
    .withCredentials()
    .then(res => {
      console.log(res);
      store.dispatch(get(res.body.profile));
    })
    .catch((err) => {
      console.log('in my catch');
      store.dispatch(getFail(err));
    });
};
