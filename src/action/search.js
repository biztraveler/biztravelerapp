import superagent from 'superagent';

export const get = info => ({
  type: 'LOCATION_GET',
  payload: info,
});

export const getLocationInfo = (location) => store => {
  console.log('location', location);
  return superagent.get(`${__BTA_URL__}/api/v1/trip/nearby?human_location=${location}`)
    .withCredentials()
    .then(res => {
      //double check this info is good
      let data = {
        amenities: res.body.amenities,
        trips: res.body.trips,
        users: res.body.users,
      };
      store.dispatch(get(data));
    })
    .catch();
};
