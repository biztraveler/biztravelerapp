const initialState = {};

export default(state = initialState, action) => {
  switch(action.type){

  case('LOGIN_FLAG'):
    return Object.assign({}, state, {
      loggedIn : action.payload
    });

  default:
    return state;
  }
};
