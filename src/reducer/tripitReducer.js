const initialState = {};

export default(state = initialState, action) => {
  switch(action.type){

  case 'TRIPIT_GET':
    return Object.assign({}, state, {
      pastTrips: action.payload,
    });

  default:
    return state;
  }
};
