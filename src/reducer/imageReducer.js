const initialState = {};

export default(state = initialState, action) => {
  switch(action.type){

  case 'UPLOAD_SUCCESS':
    return Object.assign({}, state, {
      file: action.payload,
    });
  case 'UPLOAD_FAIL':
    return Object.assign({}, state, {
      file: null,
    });

  default:
    return state;
  }
};
