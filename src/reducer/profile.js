const initialState = {};

export default(state = initialState, action) => {
  let {type,payload} = action;
  switch(type){

  case('PROFILE_GET'):
    return Object.assign({}, state, payload);

  case('TRIPIT_GET'):
    return Object.assign({}, state, payload);
    
  case('TRIPIT_GET_FAIL'):
    return Object.assign({}, state, payload);

  default:
    return state;
  }
};
