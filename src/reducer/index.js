import {combineReducers} from 'redux';
import userReducer from './userReducer';
import tripitReducer from './tripitReducer';
import profile from './profile';
import search from './search';

export default combineReducers({
  users: userReducer,
  trips: tripitReducer,
  profile: profile,
  search: search,
});
