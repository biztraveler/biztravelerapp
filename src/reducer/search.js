const initialState = {};

export default(state = initialState, action) => {
  let {type,payload} = action;
  switch(type){
    
  case('LOCATION_GET'):
    return Object.assign({}, state, payload);
    
  default:
    return state;
  }
};
