import './_profile.scss';

import React from 'react';
import {connect} from 'react-redux';

import Navbar from '../navbar';
import MyProfile from '../my-profile';
import Places from '../places';
import Feed from '../community/feed';
import TripitImages from '../tripit-images';
import { places } from '../places/fakedata/fakemapsdata';
import * as profile from '../../action/profile.js';
import history from '../../lib/history'
import { BrowserRouter, withRouter } from 'react-router-dom';




class Profile extends React.Component {
  constructor(props){
    super(props);
    this.onClickTripit = this.onClickTripit.bind(this)
    this.handleActiveTrip = this.handleActiveTrip.bind(this)
    this.state = {
      activeTrip: {},
    }
  }

  componentWillMount(){
    console.log('happened', this.props);
    this.props.getMyProfile()
    // .catch(this.props.history.push('/login'))
    .then(() => {
      this.setState({activeTrip: this.props.profile.tripit[0]})
    })
    this.props.getMyTripitProfile();
  }

  onClickTripit() {
    this.props.getMyTripitProfile();
  }

  handleActiveTrip(e) {
    console.log(e.target);
    console.log(JSON.parse(e.target.getAttribute('data')));
    this.setState({activeTrip: JSON.parse(e.target.getAttribute('data'))});
  }

  render(){
    return(
      <React.Fragment>
        <Navbar />
        <div className='profile-page'>
          <div className='profile-sidebar'>
            <MyProfile
              user = {this.props.profile.linkedin}
            />
            <hr />
            <TripitImages />
          </div>

          {
            // this.state.activeTrip.length > 0 ?
            this.props.profile.linkedin ?
              this.props.profile.linkedin.tripitLinked ?
                this.props.profile.tripit.length > 0 ?
                  <div className='map-feed-container'>
                    {this.state.activeTrip.visitedPoint
                      ?
                      <React.Fragment>

                    <div className='map-container'>
                      <Places places={this.state.activeTrip} trip={this.state.activeTrip} longitude={Number(this.state.activeTrip.visitedPoint.lon)} latitude={Number(this.state.activeTrip.visitedPoint.lat)} feed={true}
                        latLng={ {lat:this.state.activeTrip.visitedPoint.lat, lng: this.state.activeTrip.visitedPoint.lon} }
                      />
                    </div>
                    <div className='feed-container'>
                      <Feed trips={this.props.profile.tripit} editable={true} trip={this.state.activeTrip} longitude={Number(this.state.activeTrip.visitedPoint.lon)} latitude={Number(this.state.activeTrip.visitedPoint.lat)} feed={true}
                        latLng={ {lat:this.state.activeTrip.visitedPoint.lat, lng: this.state.activeTrip.visitedPoint.lon}} handleActiveTrip={this.handleActiveTrip}></Feed>
                    </div>
                  </React.Fragment>
                    : null}
                  </div>
                  :
                  <div className='tripit-placeholder'>
                    <p>No Flight data associated with tripit acount</p>
                    <p>Check back when you have traveled</p>
                    <div onClick={this.props.refreshTrips}>Refresh Trips</div>
                  </div>
                :
                <div className='tripit-placeholder'>
                  <p>This account has not been linked with Tripit</p>
                  <p></p>
                  <a href={`${__BTA_URL__}/auth/tripit/init`}>Link Tripit</a>
                </div>
              :null
              // :null
          }
        </div>
      </React.Fragment>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    profile: state.profile,
    users: state.users,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    getMyProfile: () => dispatch(profile.getMyInfo()),
    getMyTripitProfile: () => dispatch(profile.getMyTripitInfo()),
    refreshTrips: () => dispatch(profile.refreshTrips()),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Profile));
