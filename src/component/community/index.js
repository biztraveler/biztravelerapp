import React from 'react';
import Connections from './connections';
import tripitData from '../../data/tripit.json';
import { connect } from 'react-redux';
import Search from './search';
import * as search from '../../action/search.js';
import history from '../../lib/history'
import { BrowserRouter, withRouter } from 'react-router-dom';


// console.log(tripitData);

class CommunityPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searched: false,
      places: [],
      location: { lat: 0, lng: 0 },
      bounds: [],
      address: '',
      country: '',
    };

    this.getLocation = this.getLocation.bind(this);
  }

  componentWillMount(){
  }

  getLocation(address, location, bounds) {
    if (bounds) {
      this.setState({ address, location, bounds });
    } else {
      this.setState({ address, location });
    }
    this.toggleSearch();
  }
  toggleSearch() {
    console.log(this.state.location);
    console.log(this.state.address);
    this.props.getLocationInfo(this.state.address)
      .then(() => {
        this.setState(prevState => ({ searched: !prevState.searched }));
      });

  }

  render() {
    return (
      <section className="community-page">
        {this.state.searched ? (
          <Connections
            data={this.props.search}
            location={this.state.location}
            bounds={this.state.bounds}
            address={this.state.address}
            country={this.state.country}
          />
        ) : (
          <Search getLocation={this.getLocation} />
        )}
      </section>
    );
  }
}


let mapStateToProps = (state) => {
  return {
    search: state.search,
    // trips: state.trips,
    users: state.users,
    // ammen: state.ammen,

  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    getLocationInfo: (location) => dispatch(search.getLocationInfo(location)),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CommunityPage));
