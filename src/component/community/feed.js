import React from 'react';
import PropTypes from 'prop-types';
import faker from 'faker';
import Map from '../places';

const Feed = ({ trips, editable, editTrip, handleActiveTrip }) => {
  return (
    <section>
      {trips.map((x, i) => (
        console.log(JSON.stringify(x)),
        <div className="feed-item" key={`feed-item-${i}`}>
          <section className="feed-section feed-info">
            <h3>{x.visitedName}</h3>
              <img src={x.image}></img>
          </section>
          <section className="feed-section feed-map">
            <div className="feed-map-continater">
              <Map
                feed={true}
                latLng={{
                  lat: Number(x.visitedPoint.lat),
                  lng: Number(x.visitedPoint.lon),
                }}
                latitude={x.visitedPoint.lat}
                longitude={x.visitedPoint.lon}
              />
              <p>{x.description}</p>
            </div>
            <div className='toggle' onClick={handleActiveTrip} data={JSON.stringify(x)}>X</div>
          </section>
          {/* {editable ? <div onClick={e => editTrip(e, x)}>Add Info</div> : null} */}
        </div>
      ))}
    </section>
  );
};

Feed.propTypes = {
  trips: PropTypes.array,
  editable: PropTypes.bool,
  editTrip: PropTypes.func,
  handleActiveTrip: PropTypes.func,
};

Feed.defaultProps = {
  trips: [],
  editable: false,
  editTrip: () => {},
};

export default Feed;
