import React from 'react';
import PlacesAutocomplete, {
  geocodeByPlaceId,
} from 'react-places-autocomplete';

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchForm: '',
      predictions: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }
  handleChange(searchForm) {
    this.setState({ searchForm });
  }
  handleSelect(address, placeId) {
    geocodeByPlaceId(placeId)
      .then(results => {
        const { geometry, address_components } = results[0];
        const bounds = [
          { lat: geometry.viewport.f.b, lng: geometry.viewport.b.b },
          { lat: geometry.viewport.f.f, lng: geometry.viewport.b.f },
        ];
        const country =
          address_components[address_components.length - 1].long_name;
        this.props.getLocation(address);
      })
      .catch(error => console.error(error));
  }
  render() {
    return (
      <div className="form">
        <form className="search-form">
          <PlacesAutocomplete
            value={this.state.searchForm}
            onChange={this.handleChange}
            onSelect={this.handleSelect}
            searchOptions={{ types: ['(regions)'] }}
          >
            {({
              getInputProps,
              suggestions,
              getSuggestionItemProps,
              loading,
            }) => (
              <div>
                <input
                  {...getInputProps({
                    placeholder: 'Where are you travelling?',
                    className: 'location-search-input',
                    autoFocus: true,
                  })}
                />
                <div className="autocomplete-dropdown">
                  {loading && <div>Loading...</div>}
                  {suggestions.map(suggestion => {
                    const className = suggestion.active
                      ? 'suggestion-active'
                      : 'suggestion';
                    return (
                      <div
                        key={suggestion.placeId}
                        {...getSuggestionItemProps(suggestion, { className })}
                      >
                        <span>{suggestion.description}</span>
                      </div>
                    );
                  })}
                </div>
              </div>
            )}
          </PlacesAutocomplete>
        </form>
      </div>
    );
  }
}

Search.propTypes = {};
export default Search;
