import '../my-profile/_my-profile.scss';
import React from 'react';
import PropTypes from 'prop-types';
import superagent from 'superagent';
import faker from 'faker';
import Feed from './feed';
import Map from '../places';
import MyProfile from '../my-profile';
import * as _ from '../../lib/util';

class Connections extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: this.props.location,
      showProfile: false,
      activeProfile: {},
      activeTrip: {},
      experts: [],
      trips: [],
      places: [],
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleBackClick = this.handleBackClick.bind(this);
    this.handleActiveTrip = this.handleActiveTrip.bind(this);
  }

  UNSAFE_componentWillMount(){
    const experts = [];
    this.props.data.users.map((user) => {
      experts.push(user);
    });
    this.props.data.trips.map((trip) => {
      experts.map((user) => {
        if(user.id === trip.userId){
          if(!user.trips) user.trips = [];
          user.trips.push(trip);
        }
      });
    });
    this.props.data.amenities.map((amenity) => {
      experts.map((expert) => {
        expert.trips.map((trip) => {
          if(trip.id === amenity.tripId){
            if(!trip.amenities) trip.amenities = [];
            trip.amenities.push(amenity);
          }
        });
      });
    });
    console.log(experts[0]);
    console.log('I HAPPPPNEDEDDDD');
    this.setState({
      experts,
      // experts: [exper,
      activeTrip: experts[0].trips[0],
    });
  }

  handleBackClick(){
    this.setState({showProfile: !this.state.showProfile});
  }



  handleClick(e, givenName) {
    let activeProfile = this.state.experts.find(
      expert => expert.givenName === givenName
    );
    this.setState({
      showProfile: !this.state.showProfile,
      activeProfile: activeProfile,
    });
  }

  handleActiveTrip(e) {
    console.log(e.target);
    console.log(JSON.parse(e.target.getAttribute('data')));
    this.setState({activeTrip: JSON.parse(e.target.getAttribute('data'))});
  }

  render() {
    return (
      <React.Fragment>

        <section className={'connections-container'}>
          {_.renderIf(this.state.showProfile,
            <MyProfile
              user={this.state.activeProfile}
              back={this.handleBackClick}
            />,

            <div className="community-sidebar">
              {this.state.experts.map((x, i) => {
                if (!x.image) {
                  x.image = faker.image.avatar();
                }
                return (
                  <div
                    className={`profile-wrapper ${
                      this.state.activeProfile.givenName === x.givenName
                        ? 'active'
                        : null
                    }`}
                    key={`expert-${i}`}
                    onClick={e => this.handleClick(e, x.givenName)}
                  >
                    <img className="linkedin-profile-photo" src={x.image} />
                    <h2 className="linkedin-name">
                      {x.givenName} {x.familyName}{' '}
                    </h2>
                    <h4 className="linkedin-location"> {x.humanLocation} </h4>
                    <h5 className="linkedin-job"> {x.position} </h5>
                  </div>
                );
              })}
            </div>
          )}

        </section>
        <section className={'map-feed-container'}>
          <section className='map-container'>

            <Map latitude={this.state.activeTrip.visitedPoint.lat}
              longitude={this.state.activeTrip.visitedPoint.lon}
              places={[{lng:this.state.activeTrip.visitedPoint.lon, lat:this.state.activeTrip.visitedPoint.lat, name:this.state.activeTrip.visitedName}]}
              bounds={this.props.bounds}
              trip={this.state.activeTrip}
            ></Map>
          </section>
          <section className='feed-container'>
            <Feed trips={this.props.data.trips} handleActiveTrip={this.handleActiveTrip}/>
            {/* <Feed trips={this.state.experts[0].trips}/> */}
          </section>
        </section>
      </React.Fragment>
    );
  }
}

Connections.propTypes = {
  profiles: PropTypes.array,
  bounds: PropTypes.array,
  location: PropTypes.object,
  address: PropTypes.string,
  country: PropTypes.string,
};


export default Connections;
