import './_login.scss';
import React from 'react';
import { connect } from 'react-redux';
import { loginRequest } from '../../action/loginActions';
import PropTypes from 'prop-types';


class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      error: false,
      rememberMe: false,
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitLinkedIn = this.onSubmitLinkedIn.bind(this);
  }

  onChange(e) {
    let value = e.target.value;
    let name = e.target.name;
    this.setState({ [name]: value });
  }
  onSubmit(e) {
    e.preventDefault();
    if (this.state.username && this.state.password) {
      console.log('success');
      this.setState({ error: false });
      localStorage.setItem('loggedIn', true);
      this.props.history.push('/');
    } else {
      this.setState({ error: true });
    }
  }
  onSubmitLinkedIn(){
    this.props.onSubmit();
  }
  toggleCheck(){
    this.setState(prevState => ({rememberMe: !prevState.rememberMe}));
  }
  render(){
    return(
      <section className='login-page'>
        <section className='login-container'>
          <div className='login-banner'>

          </div>
          {/* <form onSubmit={this.onSubmit}>
            <div className="input-container">
              <label>Username</label>
              <input
                className={
                  this.state.error && this.state.username === ''
                    ? 'input-error'
                    : null
                }
                value={this.state.username}
                onChange={this.onChange}
                name="username"
                placeholder="Username"
                type="text"
              />
            </div>
            <div className="input-container">
              <label>Password</label>
              <input
                className={
                  this.state.error && this.state.password === ''
                    ? 'input-error'
                    : null
                }
                value={this.state.password}
                onChange={this.onChange}
                name="password"
                placeholder="Password"
                type="password"
              />
            </div> */}
            {/* <div className="options-container">
              <div className="options-spacer">
                <div className="options-left">
                  <span>
                    <input type="checkbox" />
                  </span>
                  <span>Remember me</span>
                </div>
                <div className="options-right">
                  <span>Forgot password?</span>
                </div>
              </div>
            </div> */}
            <div className='button-container'>
              <div className='button-spacer'>
                {/* <button type='submit' onClick={this.onSubmit}>Login</button> */}
                <a className='concur-buton' href={`${__BTA_URL__}/auth/linkedin/init`} type='submit'>Login With Linkedin</a>
              </div>
            </div>
          {/* </form> */}
        </section>
      </section>
    );
  }
}

const mapStateToProps = undefined;
const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: () => {
      dispatch(loginRequest());
    },
  };
};

Login.propTypes = {
  // onSubmit: PropTypes.function,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
