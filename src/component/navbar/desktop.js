import './_desktop.scss';

import React from 'react';
import {NavLink} from 'react-router-dom';
import logo from '../../assets/logo.png';
class DesktopNav extends React.Component {
  render() {
    return (
      <div className='desktop-nav'>
        <div className='nav-wrapper'>
          <NavLink exact activeClassName='active' className='menu-item' to='/community'><img src={logo}/></NavLink>
          <NavLink exact activeClassName='active' className='menu-item' to='/community'>Community</NavLink>
          <NavLink activeClassName='active' className='menu-item' to='/profile'>Profile</NavLink>
          <NavLink activeClassName='active' className='menu-item' to='/forum'>Forum</NavLink>
        </div>
      </div>
    );
  }
}

export default DesktopNav;
