import React from 'react';
import { connect } from 'react-redux';
import { uploadFileRequest } from '../../action/imageActions';
class Trip extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showInput: false,
    };
  }
  componentWillMount() {
    let cityCountryArr = this.props.trip.PrimaryLocationAddress.address.split(
      ','
    );
    let city = cityCountryArr[0];
    let country = cityCountryArr[1];
    let arrivalDate = new Date(this.props.trip.start_date);
    let departureDate = new Date(this.props.trip.end_date);
    this.onChange = this.onChange.bind(this);
    this.onChangeFile = this.onChangeFile.bind(this);
    this.uploadImage = this.uploadImage.bind(this);
    this.addComment = this.addComment.bind(this);
    this.toggleInput = this.toggleInput.bind(this);
    this.fileToBase64String = this.fileToBase64String.bind(this);
    this.setState({
      city,
      country,
      arrivalDate,
      departureDate,
    });
  }
  uploadImage(e) {
    e.stopPropagation();
  }

  onChange(e) {
    this.setState({
      tripComment: e.target.value,
    });
  }
  addComment(e) {
    e.stopPropagation();
  }
  toggleInput(e) {
    e.stopPropagation();
    this.setState(prevState => ({
      showInput: !prevState.showInput,
    }));
  }

  fileToBase64String(file) {
    return new Promise((resolve, reject) => {
      if (!file) {
        return reject(new Error('File Required'));
      }

      const fileReader = new FileReader();

      fileReader.addEventListener('load', () => resolve(fileReader.result));
      fileReader.addEventListener('error', reject);
      fileReader.readAsDataURL(file);
      return undefined;
    });
  }

  onChangeFile(e){
    let label = document.getElementById( 'file-label' );
    let labelValue = 'Chose a file';
    let fileName = '';

    if(e.target.files && e.target.files.length > 1){
      fileName = ( e.target.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', e.target.files.length );
    }else{
      fileName = e.target.value.split( '\\' ).pop();
    }
    fileName ? label.innerHTML = fileName : label.innerHTML = labelValue;
    const fileReader = new FileReader();
    e.target.files ? this.setState({fileSelected: true, files:e.target.files[0]}) : this.setState({fileSelected: false, files: null});

    // this.fileToBase64String(e.target.files[0])
    // .then((preview) => {
    //   console.log('preview', preview);
    //   preview ? this.setState({fileSelected: true, files:preview}) : this.setState({fileSelected: false, files: null});
    // })
  }

  uploadImage(e){
    e.stopPropagation();
    console.log('upload');
    console.log(this.state.fileSelected);
    console.log(this.state.files);
  }

  render(){
    console.log(this.props.trip);
    return(
      <div className='trip-container'>
        <section className='trip-content'>
          <section className='button-content-container'>
            <div className='content-continer'>
              <h3>{`Trip to ${this.state.city}, ${this.state.country} `}</h3>
              <div>
                <h4 className="arrival-departure-label">Arrived</h4>
                <h4 className="inline">{`${this.state.arrivalDate.toDateString()}`}</h4>
              </div>
              <div>
                <h4 className="arrival-departure-label">Departed</h4>
                <h4 className="inline">{`${this.state.departureDate.toDateString()}`}</h4>
              </div>
              {this.state.showInput ? (
                <div>
                  <input type="text" onChange={this.onChange} />
                </div>
              ) : null}
            </div>
            <div className='button-container'>
              <input id='file-input' type='file' name='file-input' data-multiple-caption="{count} files selected" multiple className='trip-button' onChange={(e) => this.onChangeFile(e)}></input>
              { this.state.fileSelected ?
                <div className='trip-button' onClick={() => this.props.onSubmit(this.state.files)}>Upload Image</div>
                : null
              }
              <label id='file-label' htmlFor='file-input'>Chose a file</label>
              <div className='trip-button' onClick={(e) => this.toggleInput(e)}>Add Trip Details</div>
              <div className='trip-button close-modal-button ' onClick={(e) => this.props.onClick(e)}>Done</div>
            </div>
          </section>
        </section>
      </div>
    );
  }
}

const mapStateToProps = undefined;
const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: (files) => {
      console.log('in req');
      dispatch(uploadFileRequest(files));
    },
  };
};



export default connect(mapStateToProps, mapDispatchToProps)(Trip);
