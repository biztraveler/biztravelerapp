import React from 'react';
import PropTypes from 'prop-types';
import { Marker, InfoWindow } from 'react-google-maps';
import faker from 'faker';
import './_Marker.scss';

export default class TripMarker extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false, animation: window.google.maps.Animation.DROP };
    this.toggleOpen = this.toggleOpen.bind(this);
    this.toggleBounce = this.toggleBounce.bind(this);
  }
  toggleBounce(openState, stop = false) {
    let bounce = null;
    if (openState && !stop) {
      bounce = window.google.maps.Animation.BOUNCE;
      setTimeout(this.toggleBounce, 2000, openState, true);
    }
    this.setState({ animation: bounce });
  }
  toggleOpen() {
    const openState = !this.state.open;
    this.toggleBounce(openState);
    this.setState({ open: openState });
  }
  render() {
    return (
      <Marker
        position={this.props.position}
        onClick={this.toggleOpen}
        animation={this.state.animation}
      >
        {this.state.open && (
          <InfoWindow onCloseClick={this.toggleOpen}>
            <div className="info-box">
              <div className="info-box-text">
                <h3 className="info-box-text">{this.props.name}</h3>
                <h2 className="info-box-text">local {this.props.type}</h2>
                <a href="">Visited by {this.props.visitedByFullName}</a>
              </div>
              <img
                className="profile-pic"
                height="100"
                width="100"
                src={faker.image.nightlife()}
              />
            </div>
          </InfoWindow>
        )}
      </Marker>
    );
  }
}

TripMarker.propTypes = {
  position: PropTypes.object,
  user: PropTypes.object,
  name: PropTypes.string,
  visitedByFullName: PropTypes.string,
};
