import React from 'react';
import PropTypes from 'prop-types';
import TripsMap from './Map';
export default class MapContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <TripsMap
        feed={this.props.feed}
        latLng={this.props.latLng}
        latitude={this.props.latitude}
        longitude={this.props.longitude}
        places={this.props.places}
        trip={this.props.trip}
        bounds={this.props.bounds}
        googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${__GOOGLE_KEY__}&v=3.exp&libraries=geometry,drawing,places`}
        loadingElement={<div style={{ height: '100%' }} />}
        containerElement={<div style={{ height: '100%', width: '100%' }} />}
        mapElement={<div style={{ height: '100%' }} />}
        isMarkerShown
      />
    );
  }
}

MapContainer.propTypes = {
  places: PropTypes.array,
  bounds: PropTypes.array,
  feed: PropTypes.bool,
  latLng: PropTypes.object,
  latitude: PropTypes.number,
  longitude: PropTypes.number,
};
