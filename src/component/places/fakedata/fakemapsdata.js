import faker from 'faker';

module.exports = {
  places: [
    {
      name: 'Seattle',
      lat: 47.6062,
      lng: -122.3321,
      user: { pic: faker.image.avatar(), userFullName: faker.name.findName() },
    },
    {
      name: 'Portland',
      lat: 45.5122,
      lng: -122.6587,
      user: { pic: faker.image.avatar(), userFullName: faker.name.findName() },
    },
    {
      name: 'Sacramento',
      lat: 38.5816,
      lng: -121.4944,
      user: { pic: faker.image.avatar(), userFullName: faker.name.findName() },
    },
    {
      name: 'Redding',
      lat: 40.5865,
      lng: -122.3917,
      user: { pic: faker.image.avatar(), userFullName: faker.name.findName() },
    },
  ],
  myRecentTrips: [
    {
      name: 'Bangladesh',
      lat: 23.685,
      lng: 90.3563,
    },
    {
      name: 'Bhutan',
      lat: 27.5142,
      lng: 90.4336,
    },
    {
      name: 'Tokyo',
      lat: 35.6895,
      lng: 139.6917,
    },
  ],
};
