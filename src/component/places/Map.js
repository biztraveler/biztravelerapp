import React from 'react';
import PropTypes from 'prop-types';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';
import TripMarker from './Marker';
import uniqueId from 'lodash/uniqueId';

class TripsMap extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const bounds = new window.google.maps.LatLngBounds();
    this.props.bounds.forEach(bound => {
      const { lat, lng } = bound;
      bounds.extend({ lat, lng });
    });
    console.log(this.props);
    return (
      <div>
        {!this.props.feed && (
          <GoogleMap
            // ref={map => map && map.fitBounds(bounds)}
            defaultZoom={10}
            defaultCenter={{
              lat: Number(this.props.latitude),
              lng: Number(this.props.longitude),
            }}
            center={{
              lat: Number(this.props.latitude),
              lng: Number(this.props.longitude),
            }}
          >
            {this.props.places.map((place, i) => (
              console.log('this is what im looking for', place),
              <TripMarker
                key={i}
                position={{ lat: place.lat, lng: place.lng }}
                name={place.name}
                // type={place.type}
                visitedByFullName={place.visitedByFullName}
              />
            ))}
            { this.props.trip.amenities ?
              this.props.trip.amenities.map((amenity, i) => (
              // console.log('this is what im looking for', place),
              <TripMarker
                key={i}
                position={{ lat: amenity.point.lat, lng: amenity.point.lon }}
                name={amenity.name}
                type={amenity.type}
                // visitedByFullName={place.visitedByFullName}
              />
            ))
            :
            null
          }
          </GoogleMap>
        )}
        {this.props.feed && (
          <React.Fragment>

          <GoogleMap defaultZoom={6}   defaultCenter={{
              lat: Number(this.props.latitude),
              lng: Number(this.props.longitude),
            }}
            center={{
              lat: Number(this.props.latitude),
              lng: Number(this.props.longitude),
            }}>
            <TripMarker key={`${uniqueId()}`} position={this.props.latLng} />
          </GoogleMap>
        </React.Fragment>
        )}
      </div>
    );
  }
}

TripsMap.propTypes = {
  places: PropTypes.array,
  bounds: PropTypes.array,
  isMarkerShown: PropTypes.bool,
  viewport: PropTypes.object,
  feed: PropTypes.bool,
  latLng: PropTypes.object,
  latitude: PropTypes.number,
  longitude: PropTypes.number,
};

TripsMap.defaultProps = {
  feed: false,
  isMarkerShown: true,
};

export default withScriptjs(withGoogleMap(TripsMap));
