import React from 'react';
import PropTypes from 'prop-types';
import MapContainer from './MapContainer';

const Places = ({ places, bounds, feed, latLng , latitude, longitude, trip}) => {
  // console.log();
  return (
    <MapContainer places={places} bounds={bounds} feed={feed} latLng={latLng} latitude={latitude} longitude={longitude} trip={trip} />
  );
};

Places.propTypes = {
  places: PropTypes.array,
  bounds: PropTypes.array,
  feed: PropTypes.bool,
  latLng: PropTypes.object,
  latitude: PropTypes.number,
  longitude: PropTypes.number,
  trip: PropTypes.object,
};

Places.defaultProps = {
  places: [],
  bounds: [],
  feed: false,
  latLng: { lat: 0, lng: 0 },
};

export default Places;
