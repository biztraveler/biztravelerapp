import '../community/_community.scss';
import Navbar from '../navbar';
import React from 'react';

const Login = () => (
  <React.Fragment>
    <Navbar />
    <section className="community-page">
      <div className="form">
        <a
          href={`${__BTA_URL__}/auth/linkedin/init`}
          className="login-button"
        >
          {' '}
          Login with LinkedIn
        </a>
      </div>
    </section>
  </React.Fragment>
);

export default Login;
