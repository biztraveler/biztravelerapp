import './_api.scss';
import React from 'react';
import {connect} from 'react-redux';

import * as profile from '../../action/profile.js';
import * as search from '../../action/search.js';

class API extends React.Component {
  constructor(props){
    super(props);
    
    this.onSearchClick = this.onSearchClick.bind(this);
    this.onGetProfileCLick = this.onGetProfileCLick.bind(this);
  }
  
  onSearchClick(){
    // the argument passed into the function below will be where we are querying
    // backend only has data for iceland as of right now - so iceland is hardcoded in
    this.props.getLocationInfo('iceland');
  }

  onGetProfileCLick(){
    // this will get linkedin and tripit info for the user that is loggedin
    this.props.getMyProfile();
  }
  
  
  render(){
    return(
      <div className='api-container'>
        <button onClick={this.onSearchClick}> Click Me For Search API Call </button>
        <button onClick={this.onGetProfileCLick}> Click Me For Profile API Call </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    profile: state.profile,
    search: state.search,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    getMyProfile: (user) => dispatch(profile.getMyInfo(user)),
    getLocationInfo: (location) => dispatch(search.getLocationInfo(location)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(API);