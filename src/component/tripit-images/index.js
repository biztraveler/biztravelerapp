import './_tripit-images.scss';
import React from 'react';

import CoolImages from 'cool-images';

class TripitImages extends React.Component {
  render(){
    return(
      <div className='photo-holder'>
        <img src={CoolImages.one()} />
        <img src={CoolImages.one()} />
        <img src={CoolImages.one()} />
        <img src={CoolImages.one()} />
        <img src={CoolImages.one()} />
        <img src={CoolImages.one()} />
        <img src={CoolImages.one()} />
        <img src={CoolImages.one()} />
      </div>          
    );
  }
}

export default TripitImages;