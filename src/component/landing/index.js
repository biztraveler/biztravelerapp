import './_landing.scss';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Navbar from '../navbar';
import Community from '../community';

class Landing extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <Community />
      </React.Fragment>
    );
  }
}

let mapStateToProps = () => {
  return {};
};

let mapDispatchToProps = () => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Landing);
