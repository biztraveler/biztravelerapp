import '../my-profile/_.my-profile.scss';
import './_person.scss';
import React from 'react';

class Person extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    let {name, photo, location} = this.props;
    return(
      <div className='person'>
        <img className='linkedin-profile-photo' src={photo}/>
        <h2 className='linkedin-name'> {name} </h2>
        {/* <h4 className='linkedin-location'> <FontAwesomeIcon className='location-icon' icon={faMapMarkerAlt} /> {location} </h4> */}
      </div>
    );
  }
}

export default Person;