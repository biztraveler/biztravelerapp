import './_community-profile.scss';
import React from 'react';

class CommunityProfile extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    return(
      <div className='community-profile-wrapper'>
        <div className='person'>
          <img src={require('./assets/austin.jpg')}/>
          <h2> Austin Zier </h2>
        </div>
        <div className='person'>
          <img src={require('./assets/david.jpg')}/>
          <h2> David O'Rear </h2>
        </div>
        <div className='person'>
          <img src={require('./assets/judy.jpg')}/>
          <h2> Judy Sheriff </h2>
        </div>
        <div className='person'>
          <img src={require('./assets/sabrina.jpg')}/>
          <h2> Sabrina Rasmussen </h2>
        </div>
        <div className='person'>
          <img src={require('./assets/phil.jpg')}/>
          <h2> Phil Bach </h2>
        </div>
        <div className='person'>
          <img src={require('./assets/mikey.jpg')}/>
          <h2> Mikey Furgason </h2>
        </div>
      </div>
    );
  }
}

export default CommunityProfile;