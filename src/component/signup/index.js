import './_signup.scss';
import React from 'react';

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      error: false,
    };
  }

  onChange = e => {
    let value = e.target.value;
    let name = e.target.name;
    this.setState({ [name]: value });
  };
  onSubmit = e => {
    e.preventDefault();
    if (this.state.username && this.state.password) {
      this.setState({ error: false });
      localStorage.setItem('loggedIn', true);
      this.props.history.push('/');
    } else {
      this.setState({ error: true });
    }
  };
  render() {
    return (
      <section className="signup-page">
        <section className="signup-container">
          <div className="signup-banner" />
          <form onSubmit={this.onSubmit}>
            <div className="signup-container">
              <label>Username</label>
              <input
                className={
                  this.state.error && this.state.username === ''
                    ? 'input-error'
                    : null
                }
                value={this.state.username}
                onChange={this.onChange}
                name="username"
                placeholder="Username"
                type="text"
              />
            </div>
            <div className="signup-container">
              <label>Password</label>
              <input
                className={
                  this.state.error && this.state.password === ''
                    ? 'input-error'
                    : null
                }
                value={this.state.password}
                onChange={this.onChange}
                name="password"
                placeholder="Password"
                type="password"
              />
            </div>
            <div className="options-container">
              <div className="options-spacer">
                <div className="options-left">
                  <span>
                    <input type="checkbox" />
                  </span>
                  <span>Remember me</span>
                </div>
                <div className="options-right">
                  <span>Forgot password?</span>
                </div>
              </div>
            </div>
            <div className="button-container">
              <div className="button-spacer">
                <button type="submit" onClick={this.onSubmit}>
                  Signup
                </button>
                <button className="concur-buton" type="submit">
                  Signup With Concur
                </button>
              </div>
            </div>
          </form>
        </section>
      </section>
    );
  }
}

export default Signup;
