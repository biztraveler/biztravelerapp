import './_app.scss';
import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Landing from '../landing';
import Profile from '../profile';
import Community from '../community';
import Login from '../login';
import LoginLinkedIn from '../login-linkedin';
import { loginFlag } from '../../action/loginActions'
import history from '../../lib/history'


const createRedirect = (from, to) =>
  ( <Route exact path={from} render={ () => (<Redirect to={to} />) } /> );


class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loggedIn: false,
    };
    this.onRouteChanged = this.onRouteChanged.bind(this)
  }

  componentDidMount() {
    console.log(this.props);
      console.log(history.location);
      if(history.location.pathname === "/callback-linkedin-ok") {
        this.props.onLogin();
      }
    }

  onRouteChanged(){
    console.log('route changed');
  }
  render() {
    return (
      <div className="app">
        <BrowserRouter>
          <div>
            <Route exact path="/" component={LoginLinkedIn}/>
            <Route exact path="/login" component={LoginLinkedIn}/>
            <Route exact path="/community" component={Landing}/>
            <Route exact path="/profile" component={Profile} />

            { createRedirect('/callback-linkedin-ok', '/profile') }
            { createRedirect('/callback-linkedin-fail', '/') }
            { createRedirect('/callback-tripit-ok', '/profile') }
            { createRedirect('/callback-tripit-fail', '/profile') }
            { createRedirect('/callback-tripit-not-logged-in', '/') }
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

const mapStateToProps = undefined;
const mapDispatchToProps = (dispatch) => {
  return {
    onLogin: () => {
      dispatch(loginFlag());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
