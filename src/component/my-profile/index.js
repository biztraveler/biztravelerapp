import './_my-profile.scss';

import React from 'react';

import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import {faFacebookF, faTwitter, faInstagram} from '@fortawesome/fontawesome-free-brands';
import {faMapMarkerAlt} from '@fortawesome/fontawesome-free-solid';
fontawesome.config = { autoAddCss: false };

import * as _ from '../../lib/util';

const fakeUser = {
  givenName: 'Sam',
  familyName: 'Gamgee',
  humanLocation: 'The Shire',
  position: 'Personal Assistant',
};

class MyProfile extends React.Component {
  constructor(props){
    super(props);
  }
  
  render() {
    let user = this.props.user ? this.props.user : fakeUser;
    
    return (
      <div className='profile-wrapper'>
        {_.renderIf(this.props.back, 
          <button onClick={this.props.back}> Back </button>
        )}
        <img className='linkedin-profile-photo' src={user.image}/>
        <h2 className='linkedin-name'> {user.givenName} {user.familyName} </h2>
        <h4 className='linkedin-location'> <FontAwesomeIcon className='location-icon' icon={faMapMarkerAlt} /> {user.humanLocation} </h4>
        <h5 className='linkedin-job'> {user.position} </h5>
      </div>
    );
  }
}

export default MyProfile;